# CI CD simple deployment k3s Cluster

This deployment will be use Kubernetes with lightweight distribution named `k3s`.
Type application is blog application with framework from Ghost.

Implementation has provision 2 node EC2 in AWS, and setup with K3S cluster.
For loadbalancing, this system use Elastic Load Balancer from AWS.
For web connection to App, we use Nginx Ingress Controller inside Cluster, the domain used for this app is blog.devops-warrior.site
Deployment will be use devops flow with Git CI/CD, the platform use is Gitlab

# Step 1. Setup Credential on Cluster for connection from Gitlab
Execute this inside cluster or wherever u have access with kubectl command :
```
#set the service account name
export SERVICE_ACCOUNT=blog-deployer

#create the service account and bind to role cluster-admin
kubectl create serviceaccount $SERVICE_ACCOUNT
kubectl create clusterrolebinding $SERVICE_ACCOUNT --clusterrole cluster-admin --serviceaccount default:$SERVICE_ACCOUNT

#create secret from the yaml file
kubectl apply -f service-account/create-secret.yaml

#get the secret from existing $SERVICE_ACCOUNT
export KUBE_DEPLOY_SECRET_NAME=`kubectl get serviceaccount $SERVICE_ACCOUNT -o jsonpath='{.secrets[0].name}'`
export KUBE_HOST=`kubectl get ep -o jsonpath='{.items[0].subsets[0].addresses[0].ip}'`
export KUBE_API_TOKEN=`kubectl get secret $KUBE_DEPLOY_SECRET_NAME -o jsonpath='{.data.token}'|base64 --decode`
export KUBE_API_CERT=`kubectl get secret $KUBE_DEPLOY_SECRET_NAME -o jsonpath='{.data.ca\.crt}'|base64 --decode`

```
Environment variables above must set in Gitlab CI/CD Variables, with `KUBE_NAMESPACE=default` also
Note : set the `KUBE_API_CERT` as file type variables

