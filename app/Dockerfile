#use alias for build version so we can create checkpoint in every build
FROM golang:1.14-alpine AS base 

RUN mkdir /app

RUN apk update
RUN apk upgrade
RUN apk add --no-cache bash git

#install Hugo and caddy blog SSG (Static Site Generator)
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.69.1/hugo_0.69.1_Linux-32bit.tar.gz -O /usr/local/bin/hugo.tar.gz
RUN wget https://github.com/caddyserver/caddy/releases/download/v2.0.0-rc.3/caddy_2.0.0-rc.3_linux_amd64.tar.gz -O /usr/local/bin/caddy.tar.gz

WORKDIR /usr/local/bin
RUN tar -xvf hugo.tar.gz
RUN tar -xvf caddy.tar.gz

#create base image for dev with hugo only installed
FROM golang:1.14-alpine AS dev
COPY --from=base /usr/local/bin/hugo /usr/local/bin/hugo
RUN hugo version

#create base image for production with hugo and caddy installed
FROM golang:1.14-alpine AS final
COPY --from=base /usr/local/bin/hugo /usr/local/bin/hugo
COPY --from=base /usr/local/bin/caddy /usr/local/bin/caddy

#copy resource for application to run
COPY ./app/ /app/
RUN ls -lha /app/

#make sure hugo installed and build it
RUN hugo version
RUN cd /app && hugo

#check every 5s for healthcheck with timeout 5s, exit when 10 times timeout reached
HEALTHCHECK --interval=5s --timeout=5s --retries=10 \
  CMD wget --quiet --tries=1 --spider http://localhost || exit 1

WORKDIR /app/public

EXPOSE 80

#run web server with Caddy to run Hugo application
CMD ["file-server"]

ENTRYPOINT [ "caddy" ]







